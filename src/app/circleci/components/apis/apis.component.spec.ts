import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ApisComponent } from './apis.component';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('ApisComponent', () => {
    let component: ApisComponent;
    let fixture: ComponentFixture<ApisComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ApisComponent],
            schemas: [NO_ERRORS_SCHEMA]
        }).compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ApisComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});
