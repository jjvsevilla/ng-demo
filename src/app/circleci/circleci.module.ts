import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CircleciRoutingModule } from './circleci-routing.module';
import { ApisComponent } from './components/apis/apis.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
    imports: [CommonModule, SharedModule, CircleciRoutingModule],
    declarations: [ApisComponent]
})
export class CircleciModule {}
