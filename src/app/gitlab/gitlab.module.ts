import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { GitlabRoutingModule } from './gitlab-routing.module';
import { ApisComponent } from './components/apis/apis.component';
import { SharedModule } from '../shared/shared.module';

@NgModule({
    imports: [CommonModule, SharedModule, GitlabRoutingModule],
    declarations: [ApisComponent]
})
export class GitlabModule {}
