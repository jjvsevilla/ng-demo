import { Injectable } from '@angular/core';
import { ApiService } from 'src/app/shared/services/api.service';
import { HttpHeaders } from '@angular/common/http';
import { tap, map } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class GitlabService {
    baseUrl = 'http://gitlab.com/api/v4/';

    constructor(private apiService: ApiService) {}

    getProjects(userId: string, token: string) {
        const url = `${this.baseUrl}/users/${userId}/projects?Private-Token=${token}`;
        const options = {
            headers: new HttpHeaders({
                'Content-Type': 'text/plain',
                'Private-Token': token
            })
        };

        return this.apiService.get(url, options).pipe(
            tap(res => console.log('getProjects', JSON.stringify(res))),
            map(res => res)
        );
    }
}
