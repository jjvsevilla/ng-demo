import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable({
    providedIn: 'root'
})
export class ApiService {
    constructor(private httpClient: HttpClient) {}

    get(url: string, options = {}): Observable<Object> {
        return this.httpClient.get(url, options).pipe(
            map(res => res),
            catchError(err => {
                console.log('error', err);
                return throwError(err);
            })
        );
    }
}
