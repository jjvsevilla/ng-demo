import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingsComponent } from './components/settings/settings.component';
import { RenderComponent } from './components/render/render.component';

@NgModule({
    imports: [CommonModule],
    declarations: [SettingsComponent, RenderComponent],
    exports: [SettingsComponent, RenderComponent]
})
export class SharedModule {}
