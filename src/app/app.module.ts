import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { Routes, RouterModule } from '@angular/router';
import { SetupComponent } from './components/setup/setup.component';

export const appRoutes: Routes = [
    {
        path: '',
        redirectTo: '',
        pathMatch: 'full'
    },
    {
        path: '',
        component: SetupComponent
    },
    { path: 'gitlab', loadChildren: './gitlab/gitlab.module#GitlabModule' },
    { path: 'circleci', loadChildren: './circleci/circleci.module#CircleciModule' }
];

@NgModule({
    declarations: [AppComponent, SetupComponent],
    imports: [BrowserModule, RouterModule.forRoot(appRoutes)],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {}
